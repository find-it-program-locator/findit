<?php

namespace Drupal\findit\Form;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\contacts\Form\RegisterForm as ContactsRegisterForm;

/**
 * Contacts compatible user registration form.
 */
class RegisterForm extends ContactsRegisterForm {

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    /* @var \Drupal\user\UserInterface $entity */
    $entity = parent::getEntityFromRouteMatch($route_match, $entity_type_id);

    // Add the individual role as the default. Organisations are unlikely to be
    // created through register forms and in the rare cases they are, roles will
    // be exposed allowing you to switch.
    $entity->addRole('crm_org');
    // The `crm_indiv` is not used in this project, so we no need it.
    if ($entity->hasRole('crm_indiv')) {
      $entity->removeRole('crm_indiv');
    }

    return $entity;
  }

}
